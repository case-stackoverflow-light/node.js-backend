import { body, param } from 'express-validator';

export const validateId = [param('id').isInt({ gt: 0 }).trim().escape()];

export const validateQuestion = [body('question').notEmpty().isString().trim().escape()];
