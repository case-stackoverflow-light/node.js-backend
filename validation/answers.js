import { body, param } from 'express-validator';

export const validateAnswer = [
  body('questionId').notEmpty().isInt({ gt: 0 }).trim().escape(),
  body('answer').notEmpty().isString().trim().escape(),
];

export const validateId = [param('id').isInt({ gt: 0 }).trim().escape()];
