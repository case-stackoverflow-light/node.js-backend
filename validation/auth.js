import { body } from 'express-validator';

export const validateCredentials = [
  body('username').notEmpty().isString().trim().escape(),
  body('password').notEmpty().isString().trim().escape(),
];
