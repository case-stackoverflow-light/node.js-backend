import { config } from 'dotenv';
import bcrypt from 'bcrypt';
import { insertUser, getUser } from '../data/repositories/authRepo.js';
import jwt from 'jsonwebtoken';

config();

const SALT_ROUNDS = 10;

const exists = async (username) => {
  const user = await getUser(username);
  return { isExistingUser: user.length > 0, user };
};

export const login = async (username, password) => {
  const { isExistingUser, user } = await exists(username);
  if (!isExistingUser) {
    throw new Error('Invalid username.');
  }

  const isAuthenticated = await bcrypt.compare(password, user[0].password);
  if (!isAuthenticated) {
    throw new Error('Password incorrect.');
  }

  // JWT expires after 1 hour
  const JWT_EXPIRATION_TIME = Math.floor(Date.now() / 1000) + 60 * 60;
  return jwt.sign(
    {
      data: username,
      exp: JWT_EXPIRATION_TIME,
    },
    // eslint-disable-next-line no-undef
    process.env.JWT_SECRET,
  );
};

export const register = async (username, password) => {
  const { isExistingUser } = await exists(username);
  if (isExistingUser) {
    throw new Error('User already exists.');
  }

  bcrypt.hash(password, SALT_ROUNDS, async (err, hash) => {
    if (err) throw err;
    await insertUser(username, hash);
  });
};
