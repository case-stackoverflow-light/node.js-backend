export const mockedQuestions = [
  {
    answers: 1,
    created_at: '2020-11-21T17:07:29.000Z',
    id: 1,
    modified_at: '2020-11-21T17:08:52.000Z',
    question: 'How are you doing?',
    user_id: 1,
    votes: 0,
  },
];

export const mockedQuestion = {
  question: 'How are you?',
};
