import request from 'supertest';
import app from '../app.js';
import * as questionsRepo from '../data/repositories/questionsRepo.js';
import { mockedQuestions, mockedQuestion } from './mocks/questions.js';

describe('questions endpoint', () => {
  test('GET should return questions', () => {
    const getQuestions = jest
      .spyOn(questionsRepo, 'getQuestions')
      .mockResolvedValue(mockedQuestions);

    return request(app)
      .get('/api/v1/questions')
      .set('authorization', 'Bearer token')
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.body).toStrictEqual(mockedQuestions);
        expect(getQuestions).toBeCalledTimes(1);
      });
  });

  test('POST should create a question', () => {
    const insertQuestion = jest.spyOn(questionsRepo, 'insertQuestion').mockResolvedValue();

    return request(app)
      .post('/api/v1/questions')
      .set('authorization', 'Bearer token')
      .send(mockedQuestion)
      .then((res) => {
        expect(res.status).toBe(201);
        expect(insertQuestion).toBeCalledTimes(1);
        expect(insertQuestion).toBeCalledWith('John Doe', 'How are you?');
      });
  });
});
