import createError from 'http-errors';
import express, { json, urlencoded } from 'express';
import cookieParser from 'cookie-parser';
import router from './routes/v1/index.js';

const app = express();

app.use(json());
app.use(urlencoded({ extended: false }));
app.use(cookieParser());

const API_VERSION = 'v1';
app.use(`/api/${API_VERSION}/`, router);

app.use(function (req, res, next) {
  next(createError(404));
});

app.use(function (err, req, res) {
  res.status(err.status || 500);
});

export default app;
