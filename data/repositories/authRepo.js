import { parameterizedQuery } from './repo.js';

export const getUser = async (username) => {
  const sql = 'SELECT * FROM users WHERE name = ? LIMIT 1;';
  const params = [username];
  return parameterizedQuery(sql, params);
};

export const insertUser = async (username, hash) => {
  const sql = 'INSERT INTO users (name, password) VALUES(?, ?);';
  const params = [username, hash];
  return parameterizedQuery(sql, params);
};
