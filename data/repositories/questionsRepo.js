import { parameterizedQuery, query } from './repo.js';
import { getUser } from './authRepo.js';

export const getAnswersForQuestion = async (id) => {
  const sql =
    'SELECT a.*, (SELECT COUNT(CASE WHEN av.is_upvote = true THEN 1 END) - COUNT(CASE WHEN av.is_upvote = false THEN 1 END) FROM answer_votes as av WHERE a.id = av.answer_id) AS votes FROM answers AS a WHERE a.question_id = ?;';
  const params = [id];
  return parameterizedQuery(sql, params);
};

export const getPopularQuestions = async () => {
  const sql =
    'SELECT *, (SELECT COUNT(CASE WHEN qv.is_upvote = true THEN 1 END) - COUNT(CASE WHEN qv.is_upvote = false THEN 1 END) FROM question_votes as qv WHERE q.id = qv.question_id) AS votes FROM questions AS q ORDER BY votes DESC LIMIT 5;';
  return query(sql);
};

export const getQuestion = async (id) => {
  const sql =
    'SELECT q.*, (SELECT COUNT(CASE WHEN qv.is_upvote = true THEN 1 END) - COUNT(CASE WHEN qv.is_upvote = false THEN 1 END) FROM question_votes as qv WHERE q.id = qv.question_id) AS votes FROM questions AS q WHERE q.id = ?;';
  const params = [id];
  return parameterizedQuery(sql, params);
};

export const getQuestions = async () => {
  const sql =
    'SELECT q.id, q.user_id, q.question, (SELECT COUNT(a.id) FROM answers as a WHERE q.id = a.question_id) AS answers, (SELECT COUNT(CASE WHEN qv.is_upvote = true THEN 1 END) - COUNT(CASE WHEN qv.is_upvote = false THEN 1 END) FROM question_votes as qv WHERE q.id = qv.question_id) AS votes, q.created_at, q.modified_at FROM questions as q;';
  return query(sql);
};

export const insertQuestion = async (username, question) => {
  const user = await getUser(username);
  const sql = 'INSERT INTO questions (user_id, question) VALUES(?, ?);';
  const params = [user[0].id, question];
  return parameterizedQuery(sql, params);
};

export const insertQuestionVote = async (username, questionId, isUpvote) => {
  const user = await getUser(username);
  const sql = 'INSERT INTO question_votes (user_id, question_id, is_upvote) VALUES(?, ?, ?);';
  const params = [user[0].id, questionId, isUpvote];
  return parameterizedQuery(sql, params);
};
