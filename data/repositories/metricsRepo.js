import { query } from './repo.js';

export const getAverageAnswersPerUser = async () => {
  const sql =
    'SELECT ((SELECT COUNT(*) FROM answers)  / (SELECT COUNT(*) FROM users)) as averageAnswersPerUser';
  return query(sql);
};

export const getAverageQuestionsPerUser = async () => {
  const sql =
    'SELECT ((SELECT COUNT(*) FROM questions)  / (SELECT COUNT(*) FROM users)) as averageQuestionsPerUser';
  return query(sql);
};

export const getAverageVotesPerUser = async () => {
  const sql =
    'SELECT (((SELECT COUNT(*) FROM question_votes) + (SELECT COUNT(*) FROM answer_votes))  / (SELECT COUNT(*) FROM users)) as averageVotesPerUser';
  return query(sql);
};

export const getTotalAnswers = async () => {
  const sql = 'SELECT COUNT(*) as totalAnswers FROM answers;';
  return query(sql);
};

export const getTotalQuestions = async () => {
  const sql = 'SELECT COUNT(*) as totalQuestions FROM questions;';
  return query(sql);
};

export const getTotalVotes = async () => {
  const sql =
    'SELECT (SELECT COUNT(id) FROM question_votes AS qv) + (SELECT COUNT(id) FROM answer_votes AS av) AS totalVotes;';
  return query(sql);
};
