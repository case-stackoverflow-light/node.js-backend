import { parameterizedQuery } from './repo.js';
import { getUser } from './authRepo.js';

export const insertAnswer = async (username, questionId, answer) => {
  const user = await getUser(username);
  const sql = 'INSERT INTO answers (user_id, question_id, answer) VALUES(?, ?, ?);';
  const params = [user[0].id, questionId, answer];
  return parameterizedQuery(sql, params);
};

export const insertAnswerVote = async (username, answerId, isUpvote) => {
  const user = await getUser(username);
  const sql = 'INSERT INTO answer_votes (user_id, answer_id, is_upvote) VALUES(?, ?, ?);';
  const params = [user[0].id, answerId, isUpvote];
  return parameterizedQuery(sql, params);
};
