import connection from '../connection.js';

export const parameterizedQuery = (sql, params) => {
  return new Promise((resolve, reject) => {
    connection.query(sql, params, (err, res) => {
      if (err) reject(err);
      resolve(res);
    });
  });
};

export const query = (sql) => {
  return parameterizedQuery(sql, null);
};
