/* eslint-disable no-undef */
import { createConnection } from 'mysql';
import { config } from 'dotenv';

config();

const connection = createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB,
  port: process.env.DB_PORT,
});

connection.connect((err) => {
  if (err) throw err;
  console.log('DB Connection Established.');
});

export default connection;
