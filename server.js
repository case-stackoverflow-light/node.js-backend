import app from './app.js';
import { config } from 'dotenv';
config();

// eslint-disable-next-line no-undef
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Stackoverflow Light (ITP) listening on port ${port}`);
});

export default app;
