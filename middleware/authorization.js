import jwt from 'jsonwebtoken';
import { config } from 'dotenv';

config();

export const authorize = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  if (!authHeader) return res.sendStatus(401);

  const token = authHeader.split(' ')[1];
  if (!token) return res.sendStatus(401);

  // TODO: improve this implementation
  if (process.env.NODE_ENV === 'test') {
    req.user = {
      data: 'John Doe',
    };
    return next();
  }

  // eslint-disable-next-line no-undef
  jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);
    req.user = user;
    next();
  });
};
