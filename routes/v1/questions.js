import { Router } from 'express';
import { validateId, validateQuestion } from '../../validation/questions.js';
import { validationResult } from 'express-validator';
import {
  getAnswersForQuestion,
  getQuestion,
  getQuestions,
  getPopularQuestions,
  insertQuestion,
  insertQuestionVote,
} from '../../data/repositories/questionsRepo.js';

const router = Router();

router.get('/', async (req, res, next) => {
  try {
    res.json(await getQuestions());
  } catch (err) {
    next(err);
  }
});

router.post('/', validateQuestion, async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) res.sendStatus(400);

  const { question } = req.body;
  try {
    await insertQuestion(req.user.data, question);
    res.sendStatus(201);
  } catch (err) {
    next(err);
  }
});

router.get('/popular', async (req, res, next) => {
  try {
    res.json(await getPopularQuestions());
  } catch (err) {
    next(err);
  }
});

router.get('/:id', validateId, async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) res.sendStatus(400);

  const { id } = req.params;
  try {
    const question = await getQuestion(id);
    const answers = await getAnswersForQuestion(id);
    res.json({ ...question[0], answers });
  } catch (err) {
    next(err);
  }
});

router.post('/:id/upvote', validateId, async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) res.sendStatus(400);

  const { id } = req.params;
  try {
    await insertQuestionVote(req.user.data, id, true);
    res.sendStatus(201);
  } catch (err) {
    next(err);
  }
});

router.post('/:id/downvote', validateId, async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) res.sendStatus(400);

  const { id } = req.params;
  try {
    await insertQuestionVote(req.user.data, id, false);
    res.sendStatus(201);
  } catch (err) {
    next(err);
  }
});

export default router;
