import { Router } from 'express';
import { insertAnswer, insertAnswerVote } from '../../data/repositories/answersRepo.js';
import { validateAnswer, validateId } from '../../validation/answers.js';
import { validationResult } from 'express-validator';

const router = Router();

router.post('/', validateAnswer, async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) res.sendStatus(400);

  const { questionId, answer } = req.body;
  try {
    await insertAnswer(req.user.data, questionId, answer);
    res.sendStatus(201);
  } catch (err) {
    next(err);
  }
});

router.post('/:id/upvote', validateId, async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) res.sendStatus(400);

  const { id } = req.params;
  try {
    await insertAnswerVote(req.user.data, id, true);
    res.sendStatus(201);
  } catch (err) {
    next(err);
  }
});

router.post('/:id/downvote', validateId, async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) res.sendStatus(400);

  const { id } = req.params;
  try {
    await insertAnswerVote(req.user.data, id, false);
    res.sendStatus(201);
  } catch (err) {
    next(err);
  }
});

export default router;
