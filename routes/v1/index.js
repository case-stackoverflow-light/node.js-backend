import { Router } from 'express';
import authRouter from './auth.js';
import answersRouter from './answers.js';
import metricsRouter from './metrics.js';
import questionsRouter from './questions.js';
import { authorize } from '../../middleware/authorization.js';

const router = Router();

router.use('/auth', authRouter);
router.use('/answers', authorize, answersRouter);
router.use('/metrics', authorize, metricsRouter);
router.use('/questions', authorize, questionsRouter);

export default router;
