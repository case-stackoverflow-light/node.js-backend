import { Router } from 'express';
import {
  getTotalAnswers,
  getTotalQuestions,
  getTotalVotes,
  getAverageVotesPerUser,
  getAverageAnswersPerUser,
  getAverageQuestionsPerUser,
} from '../../data/repositories/metricsRepo.js';

const router = Router();

router.get('/totals', async (req, res, next) => {
  try {
    const totalAnswers = await getTotalAnswers();
    const totalQuestions = await getTotalQuestions();
    const totalVotes = await getTotalVotes();
    res.json({
      totalAnswers: totalAnswers[0].totalAnswers,
      totalQuestions: totalQuestions[0].totalQuestions,
      totalVotes: totalVotes[0].totalVotes,
    });
  } catch (err) {
    next(err);
  }
});

router.get('/averages', async (req, res, next) => {
  try {
    const averageVotesPerUser = await getAverageVotesPerUser();
    const averageQuestionsPerUser = await getAverageQuestionsPerUser();
    const averageAnswersPerUser = await getAverageAnswersPerUser();
    res.json({
      averageVotesPerUser: averageVotesPerUser[0].averageVotesPerUser,
      averageQuestionsPerUser: averageQuestionsPerUser[0].averageQuestionsPerUser,
      averageAnswersPerUser: averageAnswersPerUser[0].averageAnswersPerUser,
    });
  } catch (err) {
    next(err);
  }
});

export default router;
