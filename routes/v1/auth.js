import { Router } from 'express';
import { validateCredentials } from '../../validation/auth.js';
import { validationResult } from 'express-validator';
import { login, register } from '../../auth/authenticator.js';

const router = Router();

router.post('/login', validateCredentials, async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) res.sendStatus(400);

  const { username, password } = req.body;
  try {
    const token = await login(username, password);
    res.status(200).json({ token });
  } catch (err) {
    next(err);
  }
});

router.post('/register', validateCredentials, async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) res.sendStatus(400);

  const { username, password } = req.body;
  try {
    await register(username, password);
    res.sendStatus(201);
  } catch (err) {
    next(err);
  }
});

export default router;
