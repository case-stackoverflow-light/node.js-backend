# Node.js Backend

Documentation for the Node.js Backend API, an ITP case as part of the job interviews.

[[_TOC_]]

## Technical Architecture

### General

The API was created using Express.js, a Node.js web app framework. The underlying database is a simple MySQL relational database. I chose these technologies because I'm quite familiar with both of them, and you can get a basic project up and running in no time with Express.

### Authentication

Despite the technical requirements stating to use an OpenID connect provider for authentication, I decided to handle authentication by handling the authentication endpoints myself with a custom JWT authenticator.

On researching OpenID Connect I found that it is a leading standard for SSO and identify provision, but I have never worked with it before. I see that it allows you to easily connect with providers like Google, Twitter and more. I read that it is also based on the OAuth 2.0 protocol, and it's simplicity and ease to consume identity tokens (using also JWT) convinced me it is a great, user-friendly way to handle authentication.

I would love to dive into this matter and find out how to implement it in this exercise, but with time constraints I decided to use something I have used before and know.

### Cloud Deployment

If I were to deploy this application on a cloud provider, I would use the following services. I will describe the process as if I were to deploy the application on Azure.

First, I would have to create an Azure Web App to deploy the Express application itself. A Web App supports many back-end languages, including Node.js. We can run our app on a Linux/Windows platform, and either push our code or a containerized version of it. Using cloud deployment allows our application to easily scale up or down based on the activity of the application. CI/CD is also supported to automate the integration/deployment process.

I would also have to create an SQL Database in Azure, and make sure the application and database can talk to each other.

## Improvements

### Docker

The docker/docker-compose deployment is not completely functional. This was the first time I've used docker/docker-compose to deploy an application locally, but I got it to work eventually. However, the application cannot connect to the database, causing the application to crash.

I suspect this has something to do with the virtual networking docker uses, but I would have to further investigate why to happens, and what I can do to solve this issue.

### Tests

To test the endpoints in an end-to-end scenario, I've chosen Jest as a testing framework. To be honest, this is the first time I got into contact with end-to-end testing scenarions, but I wanted to get it to work nonetheless. During projects at school, teachers stressed the importance of TTD but we never actually had to implement these.

I spent quite some time on the setup, but I couldn't get it to work at first (some issue with ES6 modules), but I fixed that the next day. In the meantime I decided to write all routes first, so I was not able to test each feature immediately after implementing. I realise this is not OK, but I thought this was the best way to avoid losing too much time stuck on the setup.

When I got it to work, I set up a way to mock the database calls and return demo data instead. You can find 2 test scenarios inside the test folder. These two tests show that I now know how to implememt these tests in the future, and could potentially test all endpoints in the future immediately per feature.

### Error Handling

The error handling in the API is quite basic. I use the 'express-validation' package and middleware to validate incoming data in the API, and if not valid, return a HTTP Bad Request reponse.

If any other errors get thrown during data fetching, I pass the error to the default error handler of Express, being next(). I could improve this with more time to hook into the default error handler, and write my own custom error handler to decide what to do with what type of error, and how to send feedback to the client.

## Project Setup

### Local without Docker

1. Clone the repository.

#### Database Setup

2. On a local SQL database, create a database called 'stackoverflow-light'.
3. Add a user with the necessary privileges to the 'stackoverflow-light' database, with username 'stackoverflow-light-user' and password 'stackoverflow-light-password'
4. Execute the file db-script.sql from the repository in the 'stackoverflow-light' database.

#### Project Setup

5. Copy the .env file you received into the project.
6. On a terminal, go into the project folder and run the following commands:

```
npm install
npm run start
```

The API is now available via http://localhost:3000/api/v1.

### Local with Docker

To deploy the application with docker, make sure you have the Docker Desktop application.
On a terminal, go to the repository, and run the following commands:

```
docker-compose build
docker-compose up
```

**NOTE**: The docker setup does not work properly yet, and still needs to be fixed. This is my first time with docker, and I'm convinced I could get this to work with more time.

The issue is still open, connected to branch 11-enable-to-run-local-via-docker, and the current docker files can be found there.

## Routes

The following routes have all been implemented. The base address of the api is **http://localhost:3000/api/v1/**.

### Auth

Authentication handler using JWT (jsonwebtoken package).

#### POST /auth/login

Login endpoint. Returns a JWT token.

```json
{
  "token": "TOKEN"
}
```

#### POST /auth/register

Register endpoint.

```json
{
  "username": "test",
  "password": "testpw"
}
```

### Questions

Middleware: middleware/authorization.js

#### GET /questions

Get all questions.

```json
{
  "id": 1,
  "user_id": 1,
  "question": "How are you doing?",
  "votes": 0,
  "created_at": "2020-11-21T11:04:01.000Z",
  "modified_at": "2020-11-21T11:04:01.000Z"
}
```

#### POST /questions

Create a question.

```json
{
  "question": "How are you doing?"
}
```

#### GET /questions/:id

Get a specific question with answers.

```json
{
  "id": 1,
  "user_id": 1,
  "question": "How are you doing?",
  "votes": 3,
  "created_at": "2020-11-21T17:07:29.000Z",
  "modified_at": "2020-11-21T17:08:52.000Z",
  "answers": [
    {
      "id": 1,
      "question_id": 1,
      "user_id": 1,
      "answer": "Great! How are you?",
      "votes": 2,
      "created_at": "2020-11-21T17:10:15.000Z",
      "modified_at": "2020-11-21T17:10:55.000Z"
    }
  ]
}
```

#### GET /questions/popular

Get the 5 most popular questions (based on votes)

```json
{
  "totalAnswers": 4,
  "totalQuestions": 4,
  "totalVotes": 3
}
```

#### PATCH /questions/:id/upvote

Upvote a specific question.

```json
{}
```

#### PATCH /questions/:id/downvote

Downvote a specific question.

```json
{}
```

### Answers

Middleware: middleware/authorization.js

#### POST /answers

Create an answer.

```json
{
  "questionId": "1",
  "answer": "Great! How are you?"
}
```

#### PATCH /answers/:answerId/upvote

Upvote a specific answer.

```json
{}
```

#### PATCH /answers/:answerId/downvote

Downvote a specific answer.

```json
{}
```

### Metrics

Middleware: middleware/authorization.js

#### GET /metrics/totals

Get metrics involing totals.

```json
{
  "totalAnswers": 4,
  "totalQuestions": 4,
  "totalVotes": 3
}
```

#### GET /metrics/averages

Get metrics involing averages.

```json
{
  "averageVotesPerUser": 3,
  "averageQuestionsPerUser": 1.5,
  "averageAnswersPerUser": 1.5
}
```
